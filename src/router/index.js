import Vue from 'vue'
import VueRouter from 'vue-router'
import Register from '../views/register.vue'
import Login from '../views/Login.vue'
import Userinfo from '../views/usreinfo.vue'
import Edit from '../views/Edit.vue'
import Home from '../views/Home.vue'
import Article from '../views/Article.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        component: Home
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/userinfo',
        component: Userinfo,
        meta: {
            istoken: true
        }
    },
    {
        path: '/edit',
        component: Edit,
        meta: {
            istoken: true
        }
    },
    {
        path: '/article/id',
        component: Article
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    if (!sessionStorage.getItem('token') && !sessionStorage.getItem('id') && to.meta.istoken) {
        Vue.prototype.$msg.fail('请先登录！')
        router.push('/login')
        return
    }
    next()
})

export default router