import axios from 'axios'

const http = axios.create({
    baseURL: 'http://112.74.99.5:3000/web/api'
})

http.interceptors.request.use(function(config) {
    if (sessionStorage.getItem('token') && sessionStorage.getItem('id')) {
        config.headers.Authorization = 'Bearer ' + sessionStorage.getItem('token')
    }
    return config
})
http.interceptors.response.use(function(config) {
    return config
})

export default http